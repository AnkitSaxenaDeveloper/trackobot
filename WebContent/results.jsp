<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Track-O-Bot, An Intelligent Tracker</title>
      <!-- CSS -->
      <!-- CSS -->
      <link href="css/creative.min.css" rel="stylesheet">
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700">
      <link rel="stylesheet" href="assets-new/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets-new/font-awesome/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets-new/css/form-elements.css">
      <link rel="stylesheet" href="assets-new/css/style.css">
      <link rel="stylesheet" href="assets-new/css/media-queries.css">
      <link rel="stylesheet" href="assets-new/css/view.css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
        <jsp:include page="msfheader.jsp" />  
      <!-- Description -->
      <div class="description-container">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 description-title">
                  <h2>Track-O-Bot, An Intelligent Tracker</h2>
               </div>
            </div>
           
         </div>
      </div>
      <div class="container">
      </div>
      <!-- Multi Step Form -->
      <div class="msf-container">
      <div class="container" style="background-color:#f2ffff">
         <div class="row">
            <div class="col-sm-12 msf-title">
               <h3>Current Status Of Transaction</h3>
               <!--<p>Please complete the form below to get instant access to our application and all its features:</p-->
            </div>
         </div>
         <div class="row">
            <div class="col-sm-12 msf-form">
               <div class="pricingTable col-sm-6">
                  <span class="icon"><i class="fa fa-globe"></i></span>
                  <div class="col-sm-12 msf-title">
                     <h3>IOMIP</h3>
                  </div>
                  <table class="table">
                     <thead class="thead-inverse">
                       <tr>
                           <th>#</th>
                           <th>Field Name</th>
                           <th>Field Value</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <th scope="row">1</th>
                           <td>Customer ID</td>
                           <td>12345786</td>
                        </tr>
                        <tr>
                           <th scope="row">2</th>
                           <td>Oder ID</td>
                           <td>1634527808730876</td>
                        </tr>
                        <tr>
                           <th scope="row">3</th>
                           <td>Order State</td>
                           <td>Completed</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
                <div class="pricingTable col-sm-6">
                  <span class="icon"><i class="fa fa-globe"></i></span>
                  <div class="col-sm-12 msf-title">
                     <h3>OMS</h3>
                  </div>
                  <table class="table">
                     <thead class="thead-inverse">
                        <tr>
                           <th>#</th>
                           <th>Field Name</th>
                           <th>Field Value</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <th scope="row">1</th>
                           <td>Customer ID</td>
                           <td>12345786</td>
                        </tr>
                        <tr>
                           <th scope="row">2</th>
                           <td>Oder ID</td>
                           <td>1634527808730876</td>
                        </tr>
                        <tr>
                           <th scope="row">3</th>
                           <td>Order State</td>
                           <td>Completed</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
                <div class="pricingTable col-sm-6">
                  <span class="icon"><i class="fa fa-globe"></i></span>
                  <div class="col-sm-12 msf-title">
                     <h3>BBNMS</h3>
                  </div>
                  <table class="table">
                     <thead class="thead-inverse">
                        <tr>
                           <th>#</th>
                           <th>Field Name</th>
                           <th>Field Value</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <th scope="row">1</th>
                           <td>Customer ID</td>
                           <td>12345786</td>
                        </tr>
                        <tr>
                           <th scope="row">2</th>
                           <td>Oder ID</td>
                           <td>1634527808730876</td>
                        </tr>
                        <tr>
                           <th scope="row">3</th>
                           <td>Order State</td>
                           <td>Completed</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- Javascript -->
      <script src="assets-new/js/jquery-1.11.1.min.js"></script>
      <script src="assets-new/bootstrap/js/bootstrap.min.js"></script>
      <script src="assets-new/js/jquery.backstretch.min.js"></script>
      <script src="assets-new/js/scripts.js"></script>
      <!--[if lt IE 10]>
      <script src="assets-new/js/placeholder.js"></script>
      <![endif]-->
   </body>
</html>