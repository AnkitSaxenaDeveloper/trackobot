<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Amdocs - Track-O-Bot</title>

<!-- CSS -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">

<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/form-elements.css">
<link rel="stylesheet" href="css/style.css">

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
	rel='stylesheet' type='text/css'>

<!-- Plugin CSS -->
<link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

<!-- Theme CSS -->
<link href="css/creative.min.css" rel="stylesheet">
<style>
#div1,#div2 {
	float: left;
	align: center;
	width: 200px;
	height: 80px;
	margin: 200px;
	left-padding: 100px;
	border: 2px solid black;
	background-color: grey;
	color: #fff;
}

#divx {
	align: center;
	width: 250px;
	height: 60px;
	float: center;
	border: 1px solid green;
	background-color: lightblue;
	color: #000;
}

.pic-container {
	margin: 0 auto;
	white-space: nowrap;
}

.pic-row { /* As wide as it needs to be */
	height: 500px;
	overflow: auto;
}

.pic-row a {
	clear: left;
	display: block;
}

.column-left {
	float: left;
	width: 33%;
}

.column-right {
	float: right;
	width: 33%;
}

.column-center {
	display: inline-block;
	width: 33%;
}
</style>
<script>
	function allowDrop(ev) {
		ev.preventDefault();
	}

	function drag(ev) {
		ev.dataTransfer.setData("text", ev.target.id);

	}

	function drop(ev) {
		ev.preventDefault();
		var data = ev.dataTransfer.getData("text");
		ev.target.appendChild(document.getElementById(data));

	}
</script>
<script>
function preview() {
	var arr1=[];
	var selectedone=document.querySelector('input[name="1"]:checked').value;
	var selectedtwo=document.querySelector('input[name="2"]:checked').value;
	var selectedthree=document.querySelector('input[name="3"]:checked').value;
	
	
	/* alert(selectedone);
	alert(selectedtwo);
	alert(selectedthree); */
	document.getElementById("showPreview").innerHTML="<b>Selected Mapping: "+selectedone+ " >> "+selectedtwo+" >> "+selectedthree;
}</script>


</head>

<body id="page-top">
	<jsp:include page="msfheader.jsp" />  
<header>
		<!--	<div class="header-content"> -->
		<center>
			<br />
			<br />

			<h1>Configure Interfaces and API(s)</h1>
			<h3>
				<b>Please use Radio Button for Relation Mapping and Checkbox for
					Flows' View Selection</b>
			</h3>
		
			<%
				ArrayList<String> interfaces = new ArrayList<String>();
				ArrayList<ArrayList<String>> flows = new ArrayList<ArrayList<String>>();
				interfaces.add("CSI");
				interfaces.add("IOMIP");
				interfaces.add("FIRST");
				ArrayList<String> csiFlo = new ArrayList<String>();
				ArrayList<String> iomFLo = new ArrayList<String>();
				ArrayList<String> firFlo = new ArrayList<String>();
				csiFlo.add("Order Id");
				csiFlo.add("Cust Id");
				iomFLo.add("Action ID");
				iomFLo.add("BAN");
				firFlo.add("OOID");
				firFlo.add("T_ID");
				flows.add(csiFlo);
				flows.add(iomFLo);
				flows.add(firFlo);
			%>
			<form action="configure" name="conf" id="con" method="post">
				<div class="container">

					<div class="column-left">
						<div style="color: #fff; background-color: transparent"
							class="pic-container">
							<div class="pic-row">
								<h1>
									<b>CSI</b>
								</h1>

								<% int i=0,j=0;
									for (String s : flows.get(0)) {
								%>
								<div id="divx">
									<h3 style="" black" id="drag11">
										<div style="float:right ">
											<input type="radio" name="1" value="<%=s%>">
										</div>
										<div style="float: center">
											<%=s%></div>
										<div style="float: center">
											<input type="checkbox" name="1a" value="<%=s%>">

										</div>


									</h3>

								</div>

								<%
									}
								%>

							</div>


						</div>

					</div>
					<div class="column-center">


						<div style="color: #fff; background-color: transparent"
							class="pic-container">
							<div class="pic-row">
								<h1>
									<b>IOMIP</b>
								</h1>

								<%
									for (String s : flows.get(1)) {
								%>
								<div id="divx">
									<h3 style="" black" draggable="true" ondragstart="drag(event)"
										id="drag21">

										<div style="float: right">
											<input type="radio" name="2" value="<%=s%>">
										</div>
										<div style="float: center">
											<%=s%></div>
										<div style="float: center">
											<input type="checkbox" name="2" value="<%=s%>">
										</div>

									</h3>
								</div>
								<%
									}
								%>
							</div>
						</div>
					</div>


					<div class="column-right">
						<div style="color: #fff; background-color: transparent">
							<div class="pic-row">
								<h1>
									<b>OMS</b>
								</h1>
								<%
									for (String s : flows.get(2)) {
								%>
								<div id="divx" class="pic-container">

									<h3 style="" black" draggable="true" ondragstart="drag(event)"
										id="drag31">

										<div style="float: right">
											<input type="radio" name="3" value="<%=s%>">
										</div>
										<div style="float: center">
											<%=s%>
										</div>
										<div style="float: center">
											<input type="checkbox" name="3" value="<%=s%>">
										</div>
									</h3>
								</div>

								<%
									}
								%>


							</div>
						</div>
					</div>
				</div>
				<!-- <div id="div2" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)">
  <h1 style="color:black" draggable="true" ondragstart="drag(event)" id="drag1" width="88" height="31"> </h1>
</div> -->

				<center>
					<br />
					<br /> <b> <input type="button" value="Preview"
						style="background-color: #123; height: 50px" onClick="preview()"/>
						<input type="submit" value="Submit Configuration"
						style="background-color: #123; height: 50px" /></b>
				</center>
				<br />
				<div id="showPreview" ></div>
				<input type="hidden" name="mapping" value="selectedRadios">
				<input type="hidden" name="view" value="sleectd CheckBox Map">
			</form>
	</header>
	


	<!-- jQuery -->
	<script src="vendor/jquery/jquery.min.js"></script>

	<!-- Plugin JavaScript -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
	<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Theme JavaScript -->
	<script src="js/creative.min.js"></script>

	<!-- Javascript -->
	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap1.min.js"></script>
	<script src="assets/js/jquery.backstretch.min.js"></script>
	<script src="assets/js/scripts.js"></script>
</body>
</html>