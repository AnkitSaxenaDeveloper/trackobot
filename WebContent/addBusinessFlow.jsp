<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Track-O-Bot, An Intelligent Tracker</title>
      <!-- CSS -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
   href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets-new/css/form-elements.css">
      <link rel="stylesheet" href="assets-new/css/style.css">
      <link rel="stylesheet" href="assets-new/css/media-queries.css">
   </head>
   <body>
      <!-- Top menu -->
     <jsp:include page="msfheader.jsp" />  
      <!-- Description -->
      <div class="description-container">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 description-title">
                  <h2>Business Flow Registration</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 description-text">
                  <p>
                     Register with <a href="http://azmind.com"><strong>Track-O-Bot</strong></a>, track transactions easily with us!
                  </p>
                  <div class="divider-1">. . . . . . . . . . . . . . . .</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Multi Step Form -->
      <div class="msf-container">
         <div class="container" >
            <div class="row">
               <div class="col-sm-12 msf-title">
                  <h3>Fill In The Details</h3>
                  <p>Please fill the below inforamtion to onboard flow of your application:</p>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 msf-form">
                  <form role="form" action="" method="post" class="form-inline">
                     <fieldset>
                        <h4>Flow Inforamtion</h4>
                        <div class="form-group col-md-6"  id="galaxy">
                           <label for="galaxy-name">Galaxy Name:</label><br>
                           <input type="text" name="galaxy-name" class="galaxy-name form-control" id="galaxy-name">
                        </div>
                         <div class="form-group col-md-6"  id="flow">
                           <label for="flow-name">Flow Name:</label><br>
                           <input type="text" name="flow-name" class="flow-name form-control" id="flow-name">
                        </div>
                        <div class="form-group col-md-12" id="app">
                           <label for="app">Add Application :</label><br>
                           <select class="form-control " onchange="myFunction()" id="myDropDown">
                              <option selected disabled>Select...</option>
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                           </select> 
                        </div>
                        <br>
                        
                        <br/>
                        <div id="wrapper" class="col-md-12" style="display: block;float: right;"></div>
                        <!--button type="submit" class="btn">Add Flow</button-->
                        <div class="col-md-12">
                        <button type="submit" class="btn">Add Flow </button>
                        </div>
                     </fieldset>
                     
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- Javascript -->
      
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
   <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://npmcdn.com/bootstrap@4.0.0-alpha.5/dist/js/bootstrap.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-backstretch/2.0.4/jquery.backstretch.min.js"></script>
      <script src="assets-new/js/scripts.js"></script>
      <script type="text/javascript">
      	 var deletList = [];
         function myFunction(){
            var x = document.getElementById("myDropDown");
            var abc = x.options[x.selectedIndex].innerHTML;
            deletList.push(abc);
            console.log(deletList);
            var selectedVal = x.options[x.selectedIndex];
               selectedVal.setAttribute("disabled", "disabled");
    
            //x.remove(x.selectedIndex);
            var wrapper = document.getElementById("wrapper");
            var container,closeButton;
            for(var key in deletList){
               container = document.createElement('div');
               closeButton = document.createElement('div');
               closeButton.style.display = "inline-block";
               closeButton.style.float = "right";
               closeButton.style.cursor = "pointer";

               closeButton.setAttribute("onClick","enableDropDown("+x.selectedIndex + ")");
               closeButton.innerHTML = "x";
               closeButton.style.position = "relative";
               
               closeButton.style.top = "-10px";
               container.innerHTML = deletList[key];
               container.style.width = "50px";
               container.style.border = "1px solid grey";
               container.style.padding = "4px";
               container.style.display = "inline-block";
               container.append(closeButton);
               container.setAttribute("id","opt"+x.selectedIndex);
               wrapper.append(container);
               //container.append('<div class="item">' + JSON.stringify(deletList[key]) + '</div>');
            }
            deletList = [];
         }
         function enableDropDown(val){
               console.log(val);
               var x = document.getElementById("myDropDown");
            var abc = x.options[x.selectedIndex].innerHTML;
           
            var selectedVal = x.options[val];
               selectedVal.removeAttribute("disabled");

               $("#myDropDown").val('Select...');

               var wrapper = document.getElementById("wrapper");
               var child = document.getElementById("opt"+val);
               child.outerHTML = "";
               delete child;

         }
      </script>
      
   </body>
</html>