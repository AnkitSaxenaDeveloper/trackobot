<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Track-O-Bot, An Intelligent Tracker</title>
      <!-- CSS -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700">
      <link rel="stylesheet" href="assets-new/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets-new/font-awesome/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets-new/css/form-elements.css">
      <link rel="stylesheet" href="assets-new/css/style.css">
      <link rel="stylesheet" href="assets-new/css/media-queries.css">
      
      <link href="css/creative.min.css" rel="stylesheet">
      
   </head>
   <body>
      
    <jsp:include page="msfheader.jsp" />  
    
      <!-- Description -->
      <div class="description-container">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 description-title">
                  <h2>User Registration</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 description-text">
                  <p>
                     Register with <a href="index.jsp"><strong>Track-O-Bot</strong></a>, track transactions easily with us!
                  </p>
                  <div class="divider-1">. . . . . . . . . . . . . . . .</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Multi Step Form -->
      <div class="msf-container" >
         <div class="container" >
            <div class="row">
               <div class="col-sm-12 msf-title">
                  <h3>Fill In The Details</h3>
                  <p>Please fill the below information to onboard user instantly:</p>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 msf-form">
                  <form role="form" action="" method="post" class='form-inline'>
                     <fieldset>
                        <h4>User Information </h4>
                        <div class="form-group" id="app">
                           <label for="user-name">User Name:</label><br>
                           <input type="text" name="user-name" class="user-name form-control" id="user-name">
                        </div>
                        <div class="form-group" id="pass">
                           <label for="user-pass">User Password:</label><br>
                           <input type="text" name="user-pass" class="user-pass form-control" id="user-pass">
                        </div>
                        <div class="form-group" id="role">
                           <label for="role">User Role :</label><br>
                           <select class="form-control" name="assigned-role" id="assigned-role">
                              <option selected disabled hidden>Select...</option>
                              <option value="1">Admin</option>
                              <option value="2">Non-Admin</option>
                           </select>
                        </div>
                        <br>
                        <button type="submit" class="btn">Add User</button>
                     </fieldset>	
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- Javascript -->
      <script src="assets-new/js/jquery-1.11.1.min.js"></script>
      <script src="assets-new/bootstrap/js/bootstrap.min.js"></script>
      <script src="assets-new/js/jquery.backstretch.min.js"></script>
      <script src="assets-new/js/scripts.js"></script>
   </body>
</html>