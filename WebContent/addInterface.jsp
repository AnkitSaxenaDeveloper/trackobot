<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Track-O-Bot, An Intelligent Tracker</title>
      <!-- CSS -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,700">
      <link rel="stylesheet" href="assets-new/bootstrap/css/bootstrap.min.css">
      <link rel="stylesheet" href="assets-new/font-awesome/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets-new/css/form-elements.css">
      <link rel="stylesheet" href="assets-new/css/style.css">
      <link rel="stylesheet" href="assets-new/css/media-queries.css">
   </head>
   <body>
      <!-- Top menu -->
      <jsp:include page="msfheader.jsp" />  
      <!-- Description -->
      <div class="description-container">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 description-title">
                  <h2>Application Registration</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 description-text">
                  <p>
                     Register with <a href="http://azmind.com"><strong>Track-O-Bot</strong></a>, track transactions easily with us!
                  </p>
                  <div class="divider-1">. . . . . . . . . . . . . . . .</div>
               </div>
            </div>
         </div>
      </div>
      <!-- Multi Step Form -->
      <div class="msf-container">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 msf-title">
                  <h3>Fill In The Details</h3>
                  <p>Please fill the below inforamtion to onboard application instantly:</p>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12 msf-form">
                  <form role="form" action="" method="post" class="form-inline">
                     <fieldset>
                        <h4>Application Inforamtion <span class="step">(Step 1 / 7)</span></h4>
                        <div class="form-group" id="app">
                           <label for="app-name">Application Name:</label><br>
                           <input type="text" name="app-name" class="app-name form-control" id="app-name">
                        </div>
                        <div class="form-group" id="tech">
                           <label for="tech">Technology of Interaction :</label><br>
                           <select class="form-control" name="Technology" onchange="ShowHideDiv()" id="Technology">
                              <option selected disabled hidden>Select...</option>
                              <option value="1">DataBase Connection</option>
                              <option value="2">RestFul Service</option>
                              <option value="3">Soap Service</option>
                           </select>
                        </div>
                        <div class="form-group" id="db-connection" style="display: none">
                           <label for="conn-string">Connection String:</label><br>
                           <input type="text" name="connection-string" class="connection-string form-control" id="conn-string">
                        </div>
                        <div class="form-group" id="db-sql" style="display: none">
                           <label for="querry">DataBase Querry to be ran:</label><br>
                           <input type="text" name="querry" class="querry form-control" id="querry">
                        </div>
                        <div class="form-group" id="url" style="display: none">
                           <label for="url">URL:</label><br>
                           <input type="text" name="url" class="url form-control" id="url-input">
                        </div>
                        <div class="form-group" id="method" style="display: none">
                           <label for="method">Request Method:</label><br>
                           <input type="text" name="url" class="method form-control" id="method-input">
                        </div>
                        <br>
                        <button type="submit" class="btn">Add Application</button>
                     </fieldset>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <!-- Javascript -->
      <script src="assets-new/js/jquery-1.11.1.min.js"></script>
      <script src="assets-new/bootstrap/js/bootstrap.min.js"></script>
      <script src="assets-new/js/jquery.backstretch.min.js"></script>
      <script src="assets-new/js/scripts.js"></script>
      <script type="text/javascript">
         function ShowHideDiv() {
            var tech = document.getElementById("Technology");
            var constr = document.getElementById("db-connection");
			var sql = document.getElementById("db-sql");
			var url = document.getElementById("url");
			var method = document.getElementById("method");
            constr.style.display = tech.value == "1" ? "block" : "none";
            sql.style.display = tech.value == "1" ? "block" : "none";
			url.style.display = tech.value == "2" ? "block" : "none";
			method.style.display = tech.value == "2" ? "block" : "none";
         }
      </script>
   </body>
</html>