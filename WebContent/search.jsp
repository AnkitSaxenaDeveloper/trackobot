<!DOCTYPE html>
<%@ page
	import="java.util.Arrays,java.util.List,java.util.ArrayList,java.util.HashMap,java.util.Set,java.util.Iterator,java.util.Map"%>
<%@ page import="com.amdocs.trackobot.adapter.GalaxyHolder"%>
<%@ page import="com.amdocs.trackobot.adapter.BusinessFlowHolder"%>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Track-O-Bot, An Intelligent Tracker</title>

<!-- CSS -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Raleway:400,700">
<link rel="stylesheet" href="assetss/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet"
	href="assetss/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assetss/css/form-elements.css">
<link rel="stylesheet" href="assetss/css/style.css">
<link rel="stylesheet" href="assetss/css/media-queries.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->


</head>

<body>

	<jsp:include page="msfheader.jsp" />  

	<!-- Description -->
	<div class="description-container">
		<div class="container" >
			<div class="row">
				<div class="col-sm-12 description-title">
					<h2>Track-O-Bot, An Intelligent Tracker</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 description-text">
					<p>Register with <a href="index.jsp"><strong>Track-O-Bot</strong></a>, track transactions easily with us!</p>
					<div class="divider-1">. . . . . . . . . . . . . . . .</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Multi Step Form -->
	<div class="msf-container">
		<div class="container" >
			<div class="row">
				<div class="col-sm-12 msf-title">
					<h3>Fill The Below Information</h3>
					<p>Please complete the form below to track a Transaction in Business Flow :</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 msf-form">

					<form role="form" action="results.jsp" method="post" class="form-inline">

						<fieldset>
							
							<div class="form-group">
								<div class="selects-1">
									<p>Select Galaxy :</p>
									<select class="form-control" name="galaxy"
										onchange="ShowHideDiv()" id="galaxy">
										<option selected disabled hidden>Select...</option>
										<%
											/*Set s = galaxy.keySet();
																										Iterator it = s.iterator();
																										while(it.hasNext()){ 
																										String val = (String)it.next();
									*/	%>
										<option value="<%//=val%>"><%//=val%></option>
										<%
											//}
										%>
									</select>
								</div>
							</div>
							<div class="form-group" id="flows" style="display: none">
								<div class="selects-2" id="flowselect">
									<p>Select the Business Flow:</p>
								</div>
							</div>
							<div class="form-group" style="display: none" id="input">
								<label for="input">Enter your Input :</label><br> 
								<input type="text" name="input" class="input form-control" id="inputVal">
							</div>
							<br>
							<button type="submit" class="btn">Submit</button>
						</fieldset>
					</form>

				</div>
			</div>
		</div>
	</div>



	<!-- Javascript -->
	<script src="assetss/js/jquery-1.11.1.min.js"></script>
	<script src="assetss/bootstrap/js/bootstrap.min.js"></script>
	<script src="assetss/js/jquery.backstretch.min.js"></script>
	<script src="assetss/js/scripts.js"></script>
	<script type="text/javascript">
		function showinput(value) {

		}
	</script>
	<script type="text/javascript">
		function ShowHideDiv() {
			var tech = document.getElementById("galaxy");
			var constr = document.getElementById("flows");
			var input = document.getElementById("input");
			constr.style.display = "block";
			input.style.display = "block";
			$.ajax({
				url : "AjaxServlet",
				type : "POST",
				data : {
					galaxyName : tech.value
				},
				success : function(html) {
					//alert("hello - "+html);
					// $("#results").append(html);
					var myDiv = document.getElementById("flowselect");
					//Create array of options to be added
					var flowArray = html.split(",");

					var flows = document.getElementById("selectFlow");

					//Create and append select list
					if (flows) {
						// Get its parent
						parent = flows.parentNode;
						// Create the new element
						var selectList = document.createElement("select");
						selectList.id = "selectFlow";
						selectList.className = "form-control";

						myDiv.appendChild(selectList);

						//Create and append the options
						for ( var i = 0; i < flowArray.length; i++) {
							var option = document.createElement("option");
							option.value = flowArray[i].split(":")[1];
							;
							option.text = flowArray[i].split(":")[0];
							;
							selectList.appendChild(option);
						}
						//
						parent.insertBefore(selectList, flows);

						// Remove the original
						parent.removeChild(flows);
						//
						
					} else {
						var selectList = document.createElement("select");
						selectList.id = "selectFlow";
						selectList.className = "form-control";
						myDiv.appendChild(selectList);

						//Create and append the options
						for ( var i = 0; i < flowArray.length; i++) {
							var option = document.createElement("option");
							option.value = flowArray[i].split(":")[1];
							;
							option.text = flowArray[i].split(":")[0];
							;
							selectList.appendChild(option);
						}
					}
				}
			});
		}
	</script>
	<!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

</body>

</html>
