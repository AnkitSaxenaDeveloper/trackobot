  <%
	String user = null;
	String role = null;
	String isSigned = null;
	if (request.getSession() != null) {
		user = (String) request.getSession().getAttribute("username");
		role = (String) request.getSession().getAttribute("role");
		isSigned = (String) request.getSession().getAttribute("isSigned");
	}
	  user = "admin";
		 role = "admin";
		 isSigned = "true";
%>	


     <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i
						class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand page-scroll" href="index.jsp">Track-O-Bot</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
				<%if(isSigned == "true") { %> <li><a class="page-scroll" href="#">Welcome <%=user %></a></li>  <%} %>
					<li><a class="page-scroll" href="index.jsp">Home</a></li>
					<%if(isSigned == "true") { %><li><a class="page-scroll" href="index.jsp#services">Services</a></li><%} %>
					<%if(isSigned != "true") { %><li><a class="page-scroll launch-modal" href="#modal-login"
					data-modal-id="modal-login">Login</a></li><%}else{ %>
						<li><a href="logout.jsp">Logout</a></li>
				<% 	}
						%>
					
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>