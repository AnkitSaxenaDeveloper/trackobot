<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Amdocs - Track-O-Bot</title>

<!-- CSS -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap1.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome1.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
	rel='stylesheet' type='text/css'>

<!-- Plugin CSS -->
<link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

<!-- Theme CSS -->
<link href="css/creative.min.css" rel="stylesheet">
<style type="text/css">
.typewriter p {
  color: #fff;
  font-family: Default sans-serif;
  overflow: hidden; /* Ensures the content is not revealed until the animation */
  border-right: .15em solid orange; /* The typwriter cursor */
  white-space: nowrap; /* Keeps the content on a single line */
  margin: 0 auto; /* Gives that scrolling effect as the typing happens */
  letter-spacing: .15em; /* Adjust as needed */
  animation: 
    typing 4.5s steps(30, end),
    blink-caret .7s step-end infinite;
}

/* The typing effect */
@keyframes typing {
  from { width: 0 }
  to { width: 100% }
}

/* The typewriter cursor effect */
@keyframes blink-caret {
  from, to { border-color: transparent }
  50% { border-color: orange }
}
</style>

</head>

<body id="page-top" onload="StartTimers();" onmousemove="ResetTimers();">
	<%
		String user = null;
		String role = null;
		String isSigned = null;
		if (request.getSession() != null) {
			user = (String) request.getSession().getAttribute("username");
			role = (String) request.getSession().getAttribute("role");
			isSigned = (String) request.getSession().getAttribute("isSigned");
		}
	%>

	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> Menu <i
						class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand page-scroll" href="index.jsp">Track-O-Bot</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
				<%if(isSigned == "true") { %> <li><a class="page-scroll" href="#">Welcome <%=user %></a></li>  <%} %>
					<li><a class="page-scroll" href="#about">About</a></li>
					<%if(isSigned == "true") { %><li><a class="page-scroll" href="#services">Services</a></li><%} %>
					<%if(isSigned != "true") { %><li><a class="page-scroll launch-modal" href="#modal-login"
					data-modal-id="modal-login">Login</a></li><%}else{ %>
						<li><a href="logout.jsp">Logout</a></li>
				<% 	}
						%>
					<li><a class="page-scroll" href="#contact">Contact</a></li>
					
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<header>
		<div class="header-content">
			<div class="header-content-inner">
				<h1 id="homeHeading" style="font-size:80px">Track-O-Bot</h1>
				<hr><!-- <div class="typewriter">
				<p style="font-weight:bold font-size:50px">Web based tracking tool interacting with systems <br /> in a
					functional flow facilitating Data analytics & auto fixation  of
					known issues.</p></div>  -->
					<div class="css-typing" style="font-size:18px; font-weight:bold;"></div>
		<br />
				<%if(isSigned != "true") { %><a href="#" class="btn btn-default btn-xl sr-button launch-modal"
					data-modal-id="modal-login">Login</a><%} %>
			</div>
		</div>
	</header>

	<section class="bg-primary" id="about" style="background-color:#123">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 text-center">
					<h2 class="section-heading">We've got what you need!</h2>
					<hr class="light">
					<p class="text-faded">Its Scalable and Versatile, Configurable. It also serves as a Plug and Play tool which support different interfaces for Data extraction
					</p>
					<a href="#contact"
						class="page-scroll btn btn-default btn-xl sr-button">Get
						In Touch !</a>
				</div>
			</div>
		</div>
	</section>
	<%
		if (user != null && user.equalsIgnoreCase("admin")) {
	%>
	<section id="services" style="background-color:#123">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2 class="section-heading" style="color:white;   font-weight: bold;">At Your Service</h2>
					<hr class="primary">
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
						<h3 style="color:white;   font-weight: bold;"><a href="addUser.jsp" style="color:#f2d9c4">Register a User</a></h3>
						<p class="text-muted" style="color:white; "  >Register a new user to the tool.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
						<h3  style="color:white;   font-weight: bold;"><a href="addBusinessFlow.jsp" style="color:#f2d9c4">Register a Business Flow</a></h3>
						<p class="text-muted" style="color:white; " >Register a new Business Flow.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
						<h3  style="color:white;   font-weight: bold;"><a href="addInterface.jsp" style="color:#f2d9c4">Register an Interface</a></h3>
						<p class="text-muted" style="color:white; " >Register a new Interface.</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
						<h3  style="color:white;   font-weight: bold;"><a href="configure.jsp" style="color:#f2d9c4"> Configure a Business Flow</a></h3>
						<p class="text-muted" style="color:white; " >Configure the Business flow across the
							Applications.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-6 text-center">
					<div class="service-box">
						<i class="fa fa-4x fa-paper-plane text-primary sr-icons"></i>
						<h3  style="color:white;   font-weight: bold;">
							<a href="search.jsp" style="color:#f2d9c4">Ready to Track</a>
						</h3>
						<p class="text-muted" style="color:white; " >Track a Transactions in Business Flow.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<%
		}
	%>
	<section id="contact"style="color:white;   font-weight: bold;background-color:#123" >
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2 text-center">
					<h2 class="section-heading" style="color:white;   font-weight: bold;">Let's Get In Touch!</h2>
					<hr class="primary">
					<p>Decided to ease your processes with us ? That's great! Give
						us a call or send us an email and we will get back to you as soon
						as possible!</p>
				</div>
				<div class="col-lg-4 col-lg-offset-2 text-center">
					<i class="fa fa-phone fa-3x sr-contact"></i>
					<p>+91-124-44941</p>
				</div>
				<div class="col-lg-4 text-center">
					<i class="fa fa-envelope-o fa-3x sr-contact"></i>
					<p>
						<a href="mailto:trackobot@amdocs.com" style="color:white">trackobot@amdocs.com</a>
					</p>
				</div>
			</div>
		</div>
	</section>

	<!-- MODAL -->
	
	<div class="modal fade" id="modal-login" tabindex="-1" role="dialog"
		aria-labelledby="modal-login-label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h3 class="modal-title" id="modal-login-label">Login to
						Track-O-Bot</h3>
					<p>Enter your username and password to log on:</p>
				</div>

				<div class="modal-body">

					<form role="form" action="LoginServlet" method="post"
						class="login-form">
						<div class="form-group">
							<label class="sr-only" for="form-username">Username</label> <input
								type="text" name="form-username" placeholder="Username..."
								class="form-username form-control" id="form-username">
						</div>
						<div class="form-group">
							<label class="sr-only" for="form-password">Password</label> <input
								type="password" name="form-password" placeholder="Password..."
								class="form-password form-control" id="form-password">
						</div>
						<button type="submit" class="btn">Sign in!</button>
					</form>

				</div>

			</div>
		</div>
	</div>
	
	

	<!-- jQuery -->
	<script src="vendor/jquery/jquery.min.js"></script>

	<!-- Plugin JavaScript -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
	<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

	<!-- Theme JavaScript -->
	<script src="js/creative.min.js"></script>

	<!-- Javascript -->
	<script src="assets/js/jquery-1.11.1.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap1.min.js"></script>
	<script src="assets/js/jquery.backstretch.min.js"></script>
	<script src="assets/js/scripts.js"></script>
	<script type="text/javascript">
var str = 'Web based tracking tool interacting with systems in a functional flow facilitating Data analytics & auto fixation  of known issues.';

var spans = '<span>' + str.split('').join('</span><span>') + '</span>';
$(spans).hide().appendTo('.css-typing').each(function (i) {
    $(this).delay(30 * i).css({
        display: 'inline',
        opacity: 0
    }).animate({
        opacity: 1
    },100);
});
</script>

</body>
</html>