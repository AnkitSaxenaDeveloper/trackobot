<!DOCTYPE html>
<%@ page import="java.util.Arrays,java.util.List,java.util.ArrayList,java.util.HashMap,java.util.Set,java.util.Iterator"%>
<html lang="en">
<head>
 <meta http-equiv="refresh" content="0.3;url=index.jsp" />
<!-- CSS -->
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap1.min.css">
<link rel="stylesheet"
	href="assets/font-awesome/css/font-awesome1.min.css">
<link rel="stylesheet" href="assets/css/form-elements.css">
<link rel="stylesheet" href="assets/css/style.css">

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<link
	href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
	rel='stylesheet' type='text/css'>

<!-- Plugin CSS -->
<link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

<!-- Theme CSS -->


<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Logout</title>
</head>

<body>

	<%
	if (request.getSession() != null) {
		request.getSession().setAttribute("username",null);
		request.getSession().setAttribute("role",null);
		request.getSession().setAttribute("isSigned","false");
		session.invalidate(); 
	}
	else{
		
	}
	%>
	
<header>
		<div class="header-content">
			<div class="header-content-inner">
				<h1 id="homeHeading">Successfully Logged out</h1>
				<hr>
				<p>Log out successful. Redirecting you to index page.</p>
			<img src="img/l.gif" alt="Logout" height="200" width="200"/>
			</div>
		</div>
	</header>

</body>

</html>
